package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "Login";
		testDescription = "Login into leaftaps";
		testNodes="Leads";
		author = "Dhanesh";
		category = "Smoke";
		dataSheetName = "TestData";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String UN, String PWD)
	{
		new LoginPage()
		.enterUsername(UN)
		.enterPassword(PWD)
		.clickLogin()
		.clickCRMSFA();
		}
//		.clickCreateLead()
//		.enterCompanyname("")
//		.enterFirstname("")
//		.enterLastname("")
//		.clickCreateLead()
		
		
	}
	

