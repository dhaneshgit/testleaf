package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName = "Edit lead";
		testDescription = "Edit Lead in leaftaps";
		testNodes="Leads";
		author = "Dhanesh";
		category = "Smoke";
		dataSheetName = "TestData_TC003";
	}
	
	@Test(dataProvider="fetchdata")
	public void editLead(String UN, String PWD, String FirstName, String CompanyName) 
	{
		new LoginPage()
		.enterUsername(UN)
		.enterPassword(PWD)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindlead()
		.enterFirstname(FirstName)
		.clickFindleadbutton()
		.clickLead()
		.clickEditLead()
		.enterUpdatedCompanyname(CompanyName)
		.clickSubmit();
		
	}
	
	
}
