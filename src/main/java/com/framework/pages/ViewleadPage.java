package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewleadPage extends ProjectMethods{

	public ViewleadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//span[@id='viewLead_companyName_sp']") WebElement eleCompanyname;
	@FindBy(xpath="//a[text()='Edit']") WebElement eleEditButton;
	
	public ViewleadPage verifyLead(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		verifyPartialText(eleCompanyname, data);
		return this; 
	}
	public EditleadPage clickEditLead()
	{
		click(eleEditButton);
		return new EditleadPage();
	}
}













