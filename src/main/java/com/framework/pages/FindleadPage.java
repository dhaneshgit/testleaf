package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindleadPage extends ProjectMethods{

	FindleadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement elefirstName;
	@FindBy(xpath="//button[text()='Find Leads']") WebElement elefindLeadbutton;
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]") WebElement eleleadList;
	
	public FindleadPage enterFirstname(String firstName)
	{
		clearAndType(elefirstName, firstName);
		return this;
	}
	public FindleadPage clickFindleadbutton()
	{
		click(elefindLeadbutton);
		
		return this;
	}
	public ViewleadPage clickLead()
	{
		click(eleleadList);
		return new ViewleadPage();
	}
}
