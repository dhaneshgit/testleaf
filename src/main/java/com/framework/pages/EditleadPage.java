package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditleadPage extends ProjectMethods {

	EditleadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//input[@id='updateLeadForm_companyName']") WebElement eleCompanyName;
	@FindBy(xpath="//input[@name='submitButton']") WebElement eleSubmit;
	
	
	public EditleadPage enterUpdatedCompanyname(String data)
	{
		clearAndType(eleCompanyName, data);
		return this;
	}
	
	public ViewleadPage clickSubmit()
	{
		click(eleSubmit);
		return new ViewleadPage();
	}
	
	
}
